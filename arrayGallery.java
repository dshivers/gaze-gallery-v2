    import javax.swing.*;   
    import java.awt.*;
    import java.io.*;
    import java.util.ArrayList;
    import java.io.File.*;
    import javax.imageio.*;
    import java.awt.event.*;
    import java.util.Timer;
    import java.util.TimerTask;
      
    public class arrayGallery extends JFrame {  
    	
        //Initializing Components  
        private JPanel panel;
        private ImageIcon displayImage;
        public boolean mouseExited = true;
        private boolean checkTime = false;
        private Timer clickTimer;
        
        //Initializes ArrayList stored with images
    	private static ArrayList<ImageIcon> elements = populateArray();
    	
        //Setting up GUI  
        public arrayGallery() {  
        	
        //Title of the Frame or Window  
        super("GazeGallery Milestone 2.0");  

        
        //Size of the Frame or Window  
        setSize(960,720);  
      
        //Exit Property of the Frame of Window  
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 

        this.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
               
        //Constructing JPanel  
        panel = new JPanel();  //only panel
        
        //HIERARCHY:  Pane --> Panels --> Layouts/Buttons/Icons
        Container pane = getContentPane();  
              
        //Setting the layout for panel  
        GridLayout grid = new GridLayout(3,3);  
        panel.setLayout(grid);  
        for (int i = 0; i < elements.size(); i++){
        	float ratio = 0;
        	final ImageIcon original_image = arrayImage(i);
        	final String original_path = arrayImage(i).toString();
            final int original_index = elements.indexOf(arrayImage(i));
            Image icon = arrayImage(i).getImage();
            Image thumb;
            
            //Sets ratio based on if portrait or landscape
            if (original_image.getIconHeight() > original_image.getIconWidth()){
            	ratio = (float) original_image.getIconWidth() / (float) original_image.getIconHeight();
            	
                thumb = icon.getScaledInstance((int)((float).6*(ratio*(float)240)), (int)((float).6*(float)240), java.awt.Image.SCALE_SMOOTH);
            }else if(original_image.getIconWidth() > original_image.getIconHeight()){
            	
            	ratio = (float) original_image.getIconHeight() / (float) original_image.getIconWidth();
            	
                if (ratio > (float).8){ 
            		float inverse = (float)1 / ratio;
                    thumb = icon.getScaledInstance((int)((float).6 * (inverse*(float)240)),(int)((float).6*(float)240), java.awt.Image.SCALE_SMOOTH);
                }else{
                	thumb = icon.getScaledInstance((int)((float).6*(float)320), (int)((float).6*(ratio*(float)320)), java.awt.Image.SCALE_SMOOTH);
                }
            }else{	
                thumb = icon.getScaledInstance((int)((float).6*(float)240),(int)((float).6*(float)240), java.awt.Image.SCALE_SMOOTH);
            }
            
            	
            final ImageIcon thumb_image = new ImageIcon(thumb, arrayImage(i).toString());
        	
            final JButton button = new JButton(thumb_image.toString(), thumb_image);
        	
        	button.setHorizontalTextPosition(SwingConstants.CENTER);
        	button.setVerticalTextPosition(SwingConstants.BOTTOM);
        	
            //Adds thumbnails to panel cells
            panel.add(button);
            
            //on cell click, calls gazeGallery class to make new JFrame
            button.addMouseListener(
            	new MouseAdapter(){
            		public void mouseExited(MouseEvent event){
            			//mouseExited = false;	
            			stopTimer();
            		
            		
            		}
            		public void mouseEntered(MouseEvent event){  		
            			//mouseExited = true;							
            			//checkTimeLabel(event, greed);	
            			startTimer(2000, button);
            		
            		}			
            	}
            );
            button.addActionListener(  
            		new ActionListener() {     	
                           public void actionPerformed(ActionEvent event) {
                            	JFrame newframe = new gazeGallery(original_index);
                 				newframe.setVisible(true);         				
                           }  
                    }  
            );
        };
        
        //Add the panel in the container or JFrame  
        pane.add(panel, BorderLayout.CENTER);  
        
        setContentPane(pane);  
        setVisible(true); 
        
        }
        
        
        //Finds directory of Images and stores all files in ArrayList
        //  **Image directory MUST ONLY contain image files**
        public static ArrayList<ImageIcon> populateArray(){
        	String current_directory = System.getProperty("user.dir"); 
            File image_directory = new File(current_directory + "/Images");
        	File files[] = image_directory.listFiles();
        	ArrayList<ImageIcon> populated_array = new ArrayList<ImageIcon>();
        	for (File f : files){
        		populated_array.add(new ImageIcon(f.getPath(), f.getName()));
        	}
        	return populated_array;
        }
        
        //Returns image from current ArrayList index
        public ImageIcon arrayImage(int i){
        	displayImage = elements.get(i);	
        	return displayImage;
        }
        
        /**
         * Separate out UI logic into a different thread to we can tell when the mouse leaves the button.
         */
        //KNOWN BUG:  Threads do not register a new Button, end up clicking all buttons moused over!  _
        //EDIT:  Fixed.  Will keep for future in the case of new threads
        
//        public void checkTimeLabel(final MouseEvent e, final JButton label){
//        	Thread t = new Thread(){
//        		public void run(){
//        			try {
//    					Thread.sleep(2000);
//    				}catch(InterruptedException ex) {}
//    				
//    			
//    				checkTime = true;
//    				int x2 = e.getX();
//    				int y2 = e.getY();
//    				
//    				System.out.println(x2 + " " +y2);
//    				
//    				int middle_y = label.getHeight()/2;
//    				int middle_x = label.getWidth()/2;
//    				
//    				int eighth_x = label.getHeight()/8;
//    				int eighth_y = label.getWidth()/8;
//    				
//    				
//    				if ((x2 >= (middle_x - eighth_x) && x2 <= (middle_x + eighth_x)) && (y2 >= (middle_y - eighth_y) && y2 <= (middle_y + eighth_y)) && mouseExited && checkTime){	
//                		label.doClick();				
//                		checkTime = false;
//                	}
//                	
//        		}
//        	};
//        	t.start();
//        	
//        }
        
        private void startTimer(int time, final JButton button){
        	TimerTask clickTimerTask = new TimerTask(){
        		public void run() {
        			button.doClick();
        			this.cancel();
        		}
        	};
        	clickTimer = new Timer(true);
        	clickTimer.schedule(clickTimerTask, time);
        }
        
        private void stopTimer(){
        	clickTimer.cancel();
        }
        
        
        
        public static void main(String[] args) {
        	try 
            { 
        		UIManager.setLookAndFeel(									//previously clicked or original images exhibit faint blue shading
        			        UIManager.getSystemLookAndFeelClassName());
            } 
            catch(Exception e){ 
            }
        	arrayGallery jb = new arrayGallery();  
        } 
        
    }
    
  
      
    class gazeGallery extends JFrame {  
    	
        //Initializing Components  
        private JButton prevs, next, close;  
        private JPanel panel, panel2, panel3, panel4;
        private JLabel images;
        private ImageIcon displayImage;
        public boolean mouseExited = true;
        private boolean checkTime = false;
        private int i;
        private Image thumb;
        private Icon thumb_image;
        
        //Initializes ArrayList stored with images
    	private static ArrayList<ImageIcon> elements = populateArray();
    	
        //Setting up GUI  
        public gazeGallery(int picture_index) {  
        //Title of the Frame or Window  
        super("GazeGallery Milestone 1.0");  

        
        //Size of the Frame or Window  
        setSize(960,720);  
      
        //Exit Property of the Frame of Window  
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        
        //IN CASE WE WANT CUSTOM IMAGE!!
        //Toolkit tool = Toolkit.getDefaultToolkit();
        //Image image = tool.getImage("mou.jpg");
        //Cursor c = tool.createCustomCursor(image, new Point(this.getX(), this.getY()), "img");
        //this.setCursor(c);
        
        this.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
        
        //Sets starting index at index of clicked picture
        i = picture_index;
       
        
        //Preprocessing to display first image.
        images = new JLabel();
        images.setHorizontalAlignment(SwingConstants.CENTER);
        images.setText(null);
        images.setIcon(getScaled(arrayImage(i)));

        
        //Constructing JPanel  
        panel = new JPanel();  //left panel
        panel2 = new JPanel(); //middle panel
        panel3 = new JPanel();  //right panel
        panel4 = new JPanel();  //top panel
        
        
        //Constructing JButton
        prevs = new JButton("<=");
        next = new JButton("=>");
        close = new JButton("^");
        
        //Set the Labels of the JButton in uniform alignment  
        prevs.setHorizontalAlignment(SwingConstants.LEFT);
        prevs.setPreferredSize(new Dimension(80,100));
        
        next.setHorizontalAlignment(SwingConstants.RIGHT);
        next.setPreferredSize(new Dimension(80,100));			
        
        close.setHorizontalAlignment(SwingConstants.CENTER);
        close.setPreferredSize(new Dimension(100,60));			
        
        Container pane = getContentPane();  
      
        //Setting the layout for panel  
        GridLayout grid = new GridLayout(1,1);  
        panel.setLayout(grid);  
        panel.add(prevs);
      
        //Setting layout for panel2  
        GridLayout grid2 = new GridLayout(1,1);  
        panel2.setLayout(grid2);
        panel2.add(images);
        
        //Setting the layout for panel3  
        GridLayout grid3 = new GridLayout(1,1);  
        panel3.setLayout(grid3);  
        panel3.add(next);
        
        //Setting the layout for panel4
        GridLayout grid4 = new GridLayout(1,1);
        panel4.setLayout(grid4);
        panel4.add(close);
        
        //Add the panel and panel2 in the container or JFrame  
        pane.add(panel, BorderLayout.WEST);  
        pane.add(panel2, BorderLayout.CENTER);
        pane.add(panel3, BorderLayout.EAST);  
        pane.add(panel4, BorderLayout.NORTH);
        
        setContentPane(pane);  
        setVisible(true); 
        
        
        //Add mouseclick to hovering over previous button      
        prevs.addMouseListener(
        		new MouseAdapter() {
        			public void mouseExited(MouseEvent e){
        				mouseExited = false;
        			}
        		
        			public void mouseEntered(MouseEvent e){
        				mouseExited = true;
        				checkTime(e, prevs);
        			}
        		}
        );        
        prevs.addActionListener(  
                new ActionListener() {     	
                    public void actionPerformed(ActionEvent event) {
                         images.setText(null);
                         prevsButton();
                         images.setIcon(getScaled(arrayImage(i)));
                        }  
                    }  
            );
      
        //Adds mouseclick to hovering over next button
        next.addMouseListener(
        		new MouseAdapter() {
        			public void mouseExited(MouseEvent e){
        				mouseExited = false;
        			}
        		
        			public void mouseEntered(MouseEvent e){
        				mouseExited = true;
        				checkTime(e, next);
        			}
        		}
        ); 
        next.addActionListener(  
                new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                         images.setText(null);
                         nextButton();
                         images.setIcon(getScaled(arrayImage(i)));
                        }  
                    }  
            );
        
        //Exits window after hovering over close button
        close.addMouseListener(
        		new MouseAdapter() {
        			public void mouseExited(MouseEvent e){
        				mouseExited = false;
        			}
        		
        			public void mouseEntered(MouseEvent e){
        				mouseExited = true;
        				checkTime(e, close);
        			}
        		}
        ); 
        close.addActionListener(  
                new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                    	setVisible(false);
                        dispose();				
                        }  
                    }  
            );   
        }
        
        
        //Finds directory of Images and stores all files in ArrayList
        //  **Image directory MUST ONLY contain image files**
        public static ArrayList<ImageIcon> populateArray(){
        	String current_directory = System.getProperty("user.dir"); 
            File image_directory = new File(current_directory + "/Images");
        	File files[] = image_directory.listFiles();
        	ArrayList<ImageIcon> populated_array = new ArrayList<ImageIcon>();
        	for (File f : files){
        		populated_array.add(new ImageIcon(f.getPath(), f.getName()));
        	}
        	return populated_array;
        }
        
        //Returns image from current ArrayList index
        public ImageIcon arrayImage(int i){
        	displayImage = elements.get(i);	
        	return displayImage;
        }
               
        //Resizes images that are larger than pane size
        public Icon getScaled(ImageIcon i){  //(720, 640) --> size of center panel
        	float ratio = 0;
        	String original_path = i.toString();
            Image icon = i.getImage();
            
            
            if((i.getIconHeight() > 640) || (i.getIconWidth() > 720)){
	            
            	//Portrait
	            if (i.getIconHeight() > i.getIconWidth()){
	            	ratio = (float) i.getIconWidth() / (float) i.getIconHeight();
	       
	                thumb = icon.getScaledInstance((int)((float).6*(ratio*(float)640)), (int)((float).6*(float)640), java.awt.Image.SCALE_SMOOTH);
	            //Landscape
	            }else if(i.getIconWidth() > i.getIconHeight()){
	            	
	            	ratio = (float) i.getIconHeight() / (float) i.getIconWidth();
	            	
	                if (ratio > (float).8){ 
	            		float inverse = (float)1 / ratio;
	                    thumb = icon.getScaledInstance((int)(inverse*(float)640),(int)((float)640), java.awt.Image.SCALE_SMOOTH);
	                }else{
	                	thumb = icon.getScaledInstance((int)((float)720), (int)(ratio*(float)720), java.awt.Image.SCALE_SMOOTH);
	                }
	       
	            //1:1 ratio picture
	            }else{
	                thumb = icon.getScaledInstance((int)((float).6*(float)640),(int)((float).6*(float)640), java.awt.Image.SCALE_SMOOTH);
	            }
	            thumb_image = new ImageIcon(thumb, i.toString());
	            return thumb_image;
            
            }else{
            	return i;
            }
            
            
        }
        
        //Reduces index by 1
        //Reroutes index to end of list upon reaching beginning of list
        public int prevsButton(){
            i = i-1;
            if (i == -1){
                i = elements.size()-1;
            }
            return i;
        }
        
        //Increases index by 1
        //Reroutes index to beginning of list upon reaching end of list
        public int nextButton(){
        	i = i+1;
            if (i == elements.size()){
                i = 0;
            }
            return i;
        }
        
        /**
         * Separate out UI logic into a different thread to we can tell when the mouse leaves the button.
         */
        public void checkTime(final MouseEvent e, final JButton button){
        	Thread t = new Thread(){
        		public void run(){
        			try {
    					Thread.sleep(800);
    				}catch(InterruptedException ex) {}
    				
    				checkTime = true;
    				int x2 = e.getX();
    				int y2 = e.getY();
    				
    				int midpoint = button.getHeight() / 2;
    				int fourth = button.getHeight() / 4;
    				
    				//dynamically allows clicks in middle 1/2 of button, regardless of size
    				//reclicks if constantly on
                	if ((x2 >= 0 && x2 <= 80) && (y2 >= (midpoint - fourth) && y2 <= (midpoint + fourth)) && mouseExited && checkTime){	
                		button.doClick();
                		checkTime = false;
                		checkTime(e,button);
                	}
                	//Checks if mouse is in exit button and clicks
    				if (((x2 >= 0 && x2 <= 960) && (y2 >=0 && y2 <= 80)) && mouseExited && checkTime ){
    					button.doClick();
    					checkTime = false;
    				}
        		}
        	};
        	t.start();
        }
        
       
        
    }
    
    
    