    import javax.swing.*;   
    import java.awt.*;
    import java.io.*;
    import java.util.ArrayList;
    import java.io.File.*;
    import javax.imageio.*;
    import java.awt.event.*;
      
    public class gazeGallery extends JFrame {  
    	
        //Initializing Components  
        private JButton prevs, next, close;  
        private JPanel panel, panel2, panel3, panel4;
        private JLabel images;
        private Icon displayImage;
        public boolean mouseExited = true;
        private boolean checkTime = false;
        
        //Initializes ArrayList stored with images
    	private static ArrayList<ImageIcon> elements = populateArray();
    	
        //Setting up GUI  
        public gazeGallery() {  
        //Title of the Frame or Window  
        super("GazeGallery Milestone 1.0");  

        
        //Size of the Frame or Window  
        setSize(960,720);  
      
        //Exit Property of the Frame of Window  
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 

        
        
        //Preprocessing to display first image.
        images = new JLabel();
        images.setHorizontalAlignment(SwingConstants.CENTER);
        images.setText(null);
        images.setIcon(arrayImage(i));

        
        //Constructing JPanel  
        panel = new JPanel();  //left panel
        panel2 = new JPanel(); //middle panel
        panel3 = new JPanel();  //right panel
        panel4 = new JPanel();  //top panel
        
        //Constructing JButton
        prevs = new JButton("<=");
        next = new JButton("=>");
        close = new JButton("^");
        
        //Set the Labels of the JButton in uniform alignment  
        prevs.setHorizontalAlignment(SwingConstants.LEFT);
        next.setHorizontalAlignment(SwingConstants.RIGHT);
        close.setHorizontalAlignment(SwingConstants.CENTER);
        
        Container pane = getContentPane();  
      
        //Setting the layout for panel  
        GridLayout grid = new GridLayout(1,1);  
        panel.setLayout(grid);  
        panel.add(prevs);
      
        //Setting layout for panel2  
        GridLayout grid2 = new GridLayout(1,1);  
        panel2.setLayout(grid2);
        panel2.add(images);
        
        
        //Setting the layout for panel3  
        GridLayout grid3 = new GridLayout(1,1);  
        panel3.setLayout(grid3);  
        panel3.add(next);
        
        //Setting the layout for panel4
        GridLayout grid4 = new GridLayout(1,1);
        panel4.setLayout(grid4);
        panel4.add(close);
        
        //Add the panel and panel2 in the container or JFrame  
        pane.add(panel, BorderLayout.WEST);  
        pane.add(panel2, BorderLayout.CENTER);
        pane.add(panel3, BorderLayout.EAST);  
        pane.add(panel4, BorderLayout.NORTH);
        
        setContentPane(pane);  
        setVisible(true); 
        
        
        //Add mouseclick to hovering over previous button      
        prevs.addMouseListener(
        		new MouseAdapter() {
        			public void mouseExited(MouseEvent e){
        				System.out.println("Mouse exited");
        				mouseExited = false;
        			}
        		
        			public void mouseEntered(MouseEvent e){
        				Point x1y1 = e.getPoint();
        				System.out.println(x1y1);
        				mouseExited = true;
        				
        				checkTime(e, prevs);
        			}
        		}
        );        
        prevs.addActionListener(  
                new ActionListener() {     	
                    public void actionPerformed(ActionEvent event) {
                         images.setText(null);
                         prevsButton();
                         images.setIcon(arrayImage(i));
                        }  
                    }  
            );
      
        //Adds mouseclick to hovering over next button
        next.addMouseListener(
        		new MouseAdapter() {
        			public void mouseExited(MouseEvent e){
        				System.out.println("Mouse exited");
        				mouseExited = false;
        			}
        		
        			public void mouseEntered(MouseEvent e){
        				Point x1y1 = e.getPoint();
        				System.out.println(x1y1);
        				mouseExited = true;
        				
        				checkTime(e, next);
        			}
        		}
        ); 
        next.addActionListener(  
                new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                         images.setText(null);
                         nextButton();
                         images.setIcon(arrayImage(i));
                        }  
                    }  
            );
        
        //Exits window after hovering over close button
        close.addMouseListener(
        		new MouseAdapter() {
        			public void mouseExited(MouseEvent e){
        				System.out.println("Mouse exited");
        				mouseExited = false;
        			}
        		
        			public void mouseEntered(MouseEvent e){
        				try {
        					Thread.sleep(3000);
        				}catch(InterruptedException ex) {}
        				
        				Point x1y1 = e.getPoint();
        				System.out.println(x1y1);
        				mouseExited = true;
        				
        				checkTime(e, close);
        			}
        		}
        ); 
        close.addActionListener(  
                new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                         setVisible(false);
                         dispose();					//MAY CLOSE BOTH JFRAME WINDOWS -- NEED TO CHECK WHEN WE MAKE THE NEXT ONE!!
                        }  
                    }  
            );   
        }
        
        
        //Finds directory of Images and stores all files in ArrayList
        //  **Image directory MUST ONLY contain image files**
        public static ArrayList<ImageIcon> populateArray(){
        	String current_directory = System.getProperty("user.dir"); 
            File image_directory = new File(current_directory + "/Images");
        	File files[] = image_directory.listFiles();
        	ArrayList<ImageIcon> populated_array = new ArrayList<ImageIcon>();
        	for (File f : files){
        		populated_array.add(new ImageIcon(f.getPath()));
        	}
        	return populated_array;
        }
        
        //Returns image from current ArrayList index
        public Icon arrayImage(int i){
        	displayImage = elements.get(i);	
        	return displayImage;
        }
               
        
        int i = 0;
        //Reduces index by 1
        //Reroutes index to end of list upon reaching beginning of list
        public int prevsButton(){
            i = i-1;
            if (i == -1){
                i = elements.size()-1;
            }
            return i;
        }
        
        //Increases index by 1
        //Reroutes index to beginning of list upon reaching end of list
        public int nextButton(){
        	i = i+1;
            if (i == elements.size()){
                i = 0;
            }
            return i;
        }
        
        /**
         * Separate out UI logic into a different thread to we can tell when the mouse leaves the button.
         */
        public void checkTime(final MouseEvent e, final JButton button){
        	Thread t = new Thread(){
        		public void run(){
        			try {
    					Thread.sleep(3000);
    				}catch(InterruptedException ex) {}
    				System.out.println("setting checktime to true");
    				checkTime = true;
    				int x2 = e.getX();
    				int y2 = e.getY();
    				System.out.println("x=" + x2 + ",y=" + y2);
    				System.out.println(mouseExited);
    
    				int midpoint = button.getHeight() / 2;
    				int eighth = button.getHeight() / 8;
                //	dynamically allows clicks in middle 1/4 of button, regardless of size
    			//  button width does not change, even with smaller resolution though!!  (FIX THIS RIGHT NOW)
                	if ((x2 >= 0 && x2 <= 47) && (y2 >= (midpoint - eighth) && y2 <= (midpoint + eighth)) && mouseExited && checkTime){	
                		button.doClick();
                		checkTime = false;
                	}
                	//Checks if mouse is in exit button and clicks
    				if (((x2 >= 0 && x2 <= 960) && (y2 >=0 && y2 <= 25)) && mouseExited && checkTime ){
    					button.doClick();
    					checkTime = false;
    				}
        		}
        	};
        	t.start();
        }
        
        public static void main(String[] args) {  
        	gazeGallery issjb = new gazeGallery();  
        } 
        
    }
    
    
    